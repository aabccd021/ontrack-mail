package com.apc1.ontrackmail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;

import java.util.Properties;

@RestController
public class OntrackMailController {
    @Autowired
    private JavaMailSender sender;
    private Properties emailProperties;

    @RequestMapping("/")
    @ResponseBody
    public String sendMailButton() {
        return "<html>" +
                "<body>" +
                "<a href='/sendMail'>Send Mail</a>"+
                "</body>"+
                "</html>";

    }

    private void setMailServerProperties() {

        String emailPort = "587";//gmail's smtp port

        emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", emailPort);
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.starttls.enable", "true");

    }

    @CrossOrigin(origins = {"http://ontrack21.herokuapp.com", "http://localhost:8080"})
    @RequestMapping("/sendMail")
    public String sendMail(@RequestParam(defaultValue = "edwsibr@gmail.com") String emailTo,
                           @RequestParam(defaultValue = "This is a test message") String textMessage,
                           @RequestParam(defaultValue = "Testing") String subject) {
        setMailServerProperties();

        try {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(emailTo);
            helper.setText(textMessage);
            helper.setSubject(subject);

            sender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            return "Error while sending mail ..";
        }
        return "Mail Sent Success!";
    }
}