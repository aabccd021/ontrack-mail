package com.apc1.ontrackmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OntrackMailApplication {

    public static void main(String[] args) {
        SpringApplication.run(OntrackMailApplication.class, args);
    }

}
